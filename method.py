import csv, math, random

def getCsvDatas(filepath):
    data = []
    with open(filepath, newline="") as fichier:
        reader = csv.reader(fichier, delimiter=";")
        next(reader, None)
        for row in reader:
            data.append(row)
    return data


def getValueFromIndex(datas, index):
    result = []
    for data in datas:
        result.append(float(data[index]))
    return result


def getStimulus(datas, index):
    result = []
    for array in datas:
        copies = array.copy()
        copies.pop(index)
        tampon = []
        for copy in copies:
            tampon.append(float(copy))
        result.append(tampon)
    return result


def fonction_sigmoide(value):
    expo = math.exp(-value)
    diviseur = 1.0 + expo
    valeur = 1.0

    return valeur / diviseur


def learn(stimulus, poids_init, retour_neurone, step, iterations):
    for iteration in range(0, iterations):
        # Cas aléatoire
        random_case = random.randrange(0, len(stimulus))
        stimuli = stimulus[random_case]
        reponse_neurone = []

        # neurone A
        reponse_neurone.append(fonction_sigmoide(poids_init[0] + (stimuli[0] * poids_init[1]) + (stimuli[1] * poids_init[2])))
        # neurone B
        reponse_neurone.append(fonction_sigmoide(poids_init[0] + (stimuli[0] * poids_init[3]) + (stimuli[1] * poids_init[4])))
        # neurone Z
        reponse_neurone.append(fonction_sigmoide(poids_init[0] + (stimuli[0] * poids_init[5]) + (stimuli[1] * poids_init[6])))

        # D'après le cours : delta <- ( R[k] - s ) * s * ( 1 - s )
        delta = (retour_neurone[random_case] - reponse_neurone[2]) * ( reponse_neurone[2] * (1.0 - reponse_neurone[2]))

        # Rétro-propagation : mise à jour des poids

        #neurone A
        poids_init[1] += delta * (reponse_neurone[0] * (1 - 0 - reponse_neurone[0])) * stimuli[0] * step
        poids_init[2] += delta * (reponse_neurone[0] * (1 - 0 - reponse_neurone[0])) * stimuli[1] * step
        #neurone B
        poids_init[3] += delta * (reponse_neurone[1] * (1 - 0 - reponse_neurone[1])) * stimuli[0] * step
        poids_init[4] += delta * (reponse_neurone[1] * (1 - 0 - reponse_neurone[1])) * stimuli[1] * step
        #neurone A
        poids_init[5] += delta * (reponse_neurone[2] * (1 - 0 - reponse_neurone[2])) * reponse_neurone[1] * step
        poids_init[6] += delta * (reponse_neurone[2] * (1.0 - reponse_neurone[2])) * reponse_neurone[0] * step

    random_case = random.randrange(0, len(stimulus))
    stimuli = stimulus[random_case]
    reponse_neurone = []

    # Propagation : sortie des neurones

    # neurone A
    reponse_neurone.append(fonction_sigmoide(poids_init[0] + (stimuli[0] * poids_init[1]) + (stimuli[1] * poids_init[2])))
    # neurone B
    reponse_neurone.append(fonction_sigmoide(poids_init[0] + (stimuli[0] * poids_init[3]) + (stimuli[1] * poids_init[4])))
    # neurone Z
    reponse_neurone.append(fonction_sigmoide(poids_init[0] + (stimuli[0] * poids_init[5]) + (stimuli[1] * poids_init[6])))

    print("Retour neurone : ")
    print(reponse_neurone)
    print()
    print("poids finaux : ")
    print(poids_init)
