from method import *

donnees_csv = getCsvDatas("donnees.csv")
stimulus = getStimulus(donnees_csv, 2)
poids_init = [1]
neurones = 3
boucle = (len(stimulus[0]) * neurones)
for i in range(0, boucle):
    poids_init.append(random.random())

print("poids initiaux : ")
print(poids_init)
print()
learn(
    stimulus,
    poids_init,
    getValueFromIndex(donnees_csv, 2),
    0.01,
    1000000
)